#!/bin/bash
# Debian 11 LEMP Installer
# By Fabio Dell'Aria - fabio.dellaria@gmail.com - Started on Oct 2023
#
# How to use:
# wget -q https://gitlab.com/fabio.dellaria/lemp-debian-11-setup/-/raw/main/LEMP.sh -O LEMP.sh && chmod +x ./LEMP.sh && ./LEMP.sh

# Check if the current user is "root" otherwise restart the script with "sudo"...
[ "$(whoami)" != "root" ] && exec sudo -- "$0" "$@"

# Instruct the Script to Abort for any command error...
set -e

clear
echo "LEMP Automatic Script Installer for Debian 11"
echo "---------------------------------------------"
echo

# Check if the Server is a Debian 11 (bullseye) one...
if ! (lsb_release -d | grep -q "bullseye"); 
then 
    echo "This Script is designed to works only on Debian 11 (bullseye)!"
    echo "Your current Operatiog System is:"
    lsb_release -d
    exit 
fi

# Force the SSH Server to ignore the client Locale Settings...
echo "Fixing SSH System Locale..."
sed -i 's/AcceptEnv LANG/#AcceptEnv LANG/' /etc/ssh/sshd_config
systemctl restart ssh
echo 'Done!'
echo

# System Upgrade...
echo "System Upgrade (please be patient)..."
export DEBIAN_FRONTEND=noninteractive
apt-get update -y > /dev/null
apt-get upgrade -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" > /dev/null
echo 'Done!'
echo

